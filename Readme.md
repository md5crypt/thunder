This repo uses git LFS for graphic files, make sure you have git-lfs installed

# Showdown

```
cd showdown
npm install
npm start
http://127.0.0.1/
```

This one took a while, used my own Animator class for the animations. I wrote it some time ago after working with Unity animators.

# Wheel

```
cd wheel
npm install
npm start
http://127.0.0.1/
```

I got lazy on this one, everything is in static\index.html xD, node is used only to run an http server