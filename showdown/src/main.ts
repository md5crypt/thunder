import * as PIXI from "pixi.js"
import { Animator } from "./Animator"
import vegasAnimator from "./animators/vegasAnimator"
import letterAnimator from "./animators/letterAnimator"
import boltAnimator from "./animators/boltAnimator"

const enum CONST {
	WIDTH = 802 * 2,
	HEIGHT = 336 * 2,
	PADDING_H = 140,
	PADDING_V = 100
}

const layout = {
	sign: [0, 0, 2],
	letter1: [-116, 3, 2],
	letter2: [71, 3, 2],
	letter3: [376, 11, 2],
	letter4: [473, 7, 2],
	letter5: [700, 7, 2],
	letter6: [865, 9, 2],
	letter7: [1039, 7, 2],
	letter8: [1230, 12, 2],
	bolt: [689, -97, 2],
	mustDrop: [208, 406, 1169 / 964],
	slots: [869, -19, 2],
	vegas: [108, -21, 2]
} as const

function loadResources(app: PIXI.Application) : Promise<Partial<Record<string, PIXI.LoaderResource>>> {
	return new Promise((resolve, reject) => app.loader
		.add("bg", "gfx/header.png")
		.add("sign", "gfx/showdown-off.png")
		.add("bolt", "gfx/bolt@2x.png")
		.add("letter1", "gfx/s@2x.png")
		.add("letter2", "gfx/h@2x.png")
		.add("letter3", "gfx/o-1@2x.png")
		.add("letter4", "gfx/w-1@2x.png")
		.add("letter5", "gfx/d@2x.png")
		.add("letter6", "gfx/o-2@2x.png")
		.add("letter7", "gfx/w-2@2x.png")
		.add("letter8", "gfx/n@2x.png")
		.add("slots", "gfx/slots@2x.png")
		.add("vegas", "gfx/vegas@2x.png")
		.add("mustDrop", "gfx/must_drop.png")
		.load((_loader, resources) => resolve(resources))
		.onError.add(error => reject(error))
	)
}

function resize(app: PIXI.Application) {
	app.view.width = window.innerWidth
	app.view.height = window.innerHeight
	app.view.style.overflow = "hidden"
	app.view.style.width = window.innerWidth + "px"
	app.view.style.height = window.innerHeight + "px"
	const resolution = window.devicePixelRatio || 1
	app.renderer.resize(window.innerWidth * resolution, window.innerHeight * resolution)
	const container = app.stage.getChildAt(1) as PIXI.Container
	const scale = Math.min(app.view.width / (CONST.WIDTH + CONST.PADDING_H), app.view.height / (CONST.HEIGHT + CONST.PADDING_V))
	container.scale.set(scale)
	container.position.set(
		(CONST.PADDING_H * scale / 2) + (app.view.width - scale * (CONST.WIDTH + CONST.PADDING_H)) / 2,
		(CONST.PADDING_V * scale / 2) + (app.view.height - scale * (CONST.HEIGHT + CONST.PADDING_H)) / 2
	)
	const bg = app.stage.getChildAt(0) as PIXI.Sprite
	bg.position.set(app.view.width / 2, 0)
	bg.scale.set(Math.max(
		2 * scale,
		app.view.width / bg.texture.width,
		app.view.height / bg.texture.height
	))
}

window.addEventListener("load", async () => {
	const app = new PIXI.Application({
		backgroundColor: 0
	})
	const resources = await loadResources(app) as Record<string, PIXI.LoaderResource>
	document.body.appendChild(app.view)

	const container = new PIXI.Container()
	const sprites: Record<keyof typeof layout, PIXI.Sprite> = {} as any
	for (const [key, value] of Object.entries(layout)) {
		const sprite = new PIXI.Sprite(resources[key].texture)
		sprite.position.set(value[0], value[1])
		sprite.scale.set(value[2])
		sprites[key as "bolt"] = sprite
		container.addChild(sprite)
	}

	const bg = new PIXI.Sprite(resources.bg.texture)
	bg.anchor.set(0.5, 0)
	app.stage.addChild(bg)
	app.stage.addChild(container)

	resize(app)
	window.addEventListener("resize", () => resize(app))

	vegasAnimator(500, alpha => {
		sprites.vegas.alpha = alpha
		sprites.slots.alpha = alpha
	}).start()
	vegasAnimator(100 + 8 * 150 + 400, alpha => sprites.mustDrop.alpha = alpha).start()
	boltAnimator(alpha => sprites.bolt.alpha = alpha).start()
	for (let i = 1; i <= 8; i++) {
		letterAnimator(1000 + i * 150, alpha => sprites[("letter" + i) as "letter1"].alpha = alpha).start()
	}

	app.ticker.add(_ => {
		Animator.update(app.ticker.elapsedMS)
	})
})
