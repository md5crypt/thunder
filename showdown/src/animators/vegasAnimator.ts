import { Animator } from "../Animator"

export default (delay: number, setAlpha: (alpha: number) => void) => new Animator({
	initial: {
		duration: delay,
		animation: () => setAlpha(0),
		transition: "preBlink"
	},
	preBlink: {
		duration: 50,
		animation: context => setAlpha(context.interpolate(0, 1, "easeIn")),
		transition: "preBlinkWait"
	},
	preBlinkWait: {
		duration: 500,
		animation: () => setAlpha(1),
		transition: "blinkOff"
	},
	blinkOff: {
		duration: 0,
		animation: context => setAlpha(context.interpolate(1, 0, "easeOut")),
		transition: "blinkOffWait"
	},
	blinkOffWait: {
		duration: 20,
		animation: () => setAlpha(0),
		transition: "blinkOn"
	},
	blinkOn: {
		duration: 40,
		animation: context => setAlpha(context.interpolate(0, 1, "easeIn")),
		transition: context => {
			context.parameters.cnt += 1
			return context.parameters.cnt > 1 ? "blinkEnd" : "blinkOnWait"
		}
	},
	blinkOnWait: {
		duration: 60,
		animation: () => setAlpha(1),
		transition: "blinkOff"
	},
	blinkEnd: {
		duration: 0,
		animation: () => setAlpha(1),
		transition: "stop"
	}
}, { cnt: 0 })
