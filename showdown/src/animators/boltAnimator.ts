import { Animator } from "../Animator"

const steps = [
	{progress: 0, value: 0},
	{progress: 0.1, value: 1},
	{progress: 0.15, value: 0},
	{progress: 0.3, value: 1},
	{progress: 0.5, value: 0},
	{progress: 0.55, value: 1},
	{progress: 0.7, value: 0},
	{progress: 0.9, value: 1},
	{progress: 1, value: 0}
]

export default (setAlpha: (alpha: number) => void) => new Animator({
	initial: {
		animation: () => setAlpha(0),
		duration: 2000,
		transition: "blink"
	},
	blink: {
		animation: context => setAlpha(context.steps(steps)),
		duration: 500,
		loop: true,
		transition: context => {
			context.parameters.cnt += 1
			if (context.parameters.cnt == 20) {
				context.parameters.cnt = 0
				return "wait"
			}
			return false
		}
	},
	wait: {
		duration: 1000 * 10,
		animation: () => setAlpha(1),
		transition: "blink"
	}
}, {cnt: 0})
