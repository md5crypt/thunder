import { Animator } from "../Animator"

export default (delay: number, setAlpha: (alpha: number) => void) => new Animator({
	initial: {
		animation: () => setAlpha(0),
		duration: delay,
		transition: "turnOn"
	},
	turnOn: {
		animation: context => setAlpha(context.interpolate(0, 1, "easeIn")),
		duration: 100,
		transition: "on"
	},
	on: {
		duration: 0,
		animation: () => setAlpha(1),
		transition: "stop"
	}
})
